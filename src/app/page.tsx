import Form from "@/components/screens";

export default function Home() {
  return (
    <main>
      <div className='container'>
        <Form />
      </div>
    </main>
  );
}
